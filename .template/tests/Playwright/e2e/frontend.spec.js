// @ts-check
const { test, expect } = require('@playwright/test');

test.beforeEach(async ({ page }) => {
  await page.goto('/');
})

test('Frontend - home page', async ({ page }) => {
  await expect((await page.locator('h2').textContent()).trim()).toBe("Eucalyptus kangaroo");
  await expect(page.locator('.logo')).toBeVisible();
  await expect((await page.locator('.js-made-with').textContent()).trim()).toBe("Made with");
});

test('Frontend - menu pages', async ({ page }) => {
  let pages = ['News', 'Products'];
  for (const title of pages) {
    let newsPage = await page.getByText(title);
    await newsPage.click();
    await expect((await page.locator('.menu__active').textContent()).trim()).toBe(title);
  }
});
