// @ts-check
const { test, expect } = require('@playwright/test');

test('Backend login', async ({ page }) => {
  await page.goto('/typo3');
  await page.fill('#t3-username', 'admin');
  await page.fill('#t3-password', 'Password.1');
  await page.waitForTimeout(500);
  await page.click('#t3-login-submit');

  await page.locator('.topbar-site-name').waitFor();
  await page.waitForTimeout(1000);

  expect(await page.locator('.topbar-site-name').textContent()).toBe("TYPO3-Dev");
  await page.waitForTimeout(1000);

  await page.click('.modulemenu-name:is(:text("Page"), :text("Page"))')
  await expect(page.locator('.nodes-list > .node:nth-child(2) .node-contentlabel')).toHaveCount(1)
  await page.click('.nodes-list > .node:nth-child(2) .node-contentlabel')

  let contentFrame = await page.frameLocator('#typo3-contentIframe')
  await expect((await contentFrame.locator('typo3-backend-editable-page-title').textContent()).trim()).toBe('Welcome to TYPO3')
});
