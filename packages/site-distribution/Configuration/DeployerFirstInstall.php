<?php

namespace Deployer;

/*
 * This file is part of the TYPO3 project template.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read
 * the LICENSE file that was distributed with this source code.
 */

require_once 'recipe/common.php';
require_once 'contrib/rsync.php';
$composerConfig = json_decode(file_get_contents('./composer.json'), true, 512, JSON_THROW_ON_ERROR);

/**
 * DocumentRoot / WebRoot for the TYPO3 installation
 */
set('typo3_webroot', function () use ($composerConfig) {
    if ($composerConfig['extra']['typo3/cms']['web-dir'] ?? false) {
        return $composerConfig['extra']['typo3/cms']['web-dir'];
    }

    return 'public';
});

/**
 * Path to TYPO3 cli
 */
set('bin/typo3', function () use ($composerConfig) {
    if ($composerConfig['config']['bin-dir'] ?? false) {
        return $composerConfig['config']['bin-dir'] . '/typo3';
    }

    return 'vendor/bin/typo3';
});

/**
 * Log files to display when running `./vendor/bin/dep logs:app`
 */
set('log_files', 'var/log/typo3_*.log');

/**
 * Shared directories
 */
set('shared_dirs', [
    '{{typo3_webroot}}/fileadmin',
    '{{typo3_webroot}}/typo3temp',
    'var/session',
    'var/log',
    'var/lock',
    'var/charset',
]);

/**
 * Writeable directories
 */
set('writable_dirs', [
    '{{typo3_webroot}}/fileadmin',
    '{{typo3_webroot}}/typo3temp',
    'var',
]);

/**
 * Composer options
 */
set('composer_options', ' --no-dev --verbose --prefer-dist --no-progress --no-interaction --optimize-autoloader');

$exclude = [
    '.Build',
    '.git',
    '.gitlab',
    '.ddev',
    '.deployer',
    '.idea',
    '.DS_Store',
    '.gitlab-ci.yml',
    '.npm',
    'deploy.yaml',
    'package.json',
    'package-lock.json',
    'node_modules/',
    'var/',
    'public/fileadmin/',
    'public/typo3temp/',
    'config/system/additional.php',
];

set('rsync', [
    'exclude' => array_merge(get('shared_dirs'), get('shared_files'), $exclude),
    'exclude-file' => false,
    'include' => ['vendor'],
    'include-file' => false,
    'filter' => ['dir-merge,-n /.gitignore'],
    'filter-file' => false,
    'filter-perdir' => false,
    'flags' => 'avz',
    'options' => ['delete', 'keep-dirlinks', 'links'],
    'timeout' => 600,
]);

desc('TYPO3 - Setup the installation for the first time');
task('typo3:setup', function () {
    cd('{{release_path}}');
    run('export $(cat {{deploy_path}}/.env | xargs) && {{bin/php}} {{bin/typo3}} setup --no-interaction');
    run('rm {{deploy_path}}/.env');
});

desc('TYPO3 - Set up all extensions');
task('typo3:extension:setup', function () {
    cd('{{release_path}}');
    run('{{bin/php}} {{bin/typo3}} extension:setup');
});

desc('TYPO3 - Copy generated shared files into shared folder');
task('typo3:copy:shared', function () {
    run('mkdir -p {{deploy_path}}/shared/config/system');
    run('cp {{release_path}}/{{typo3_webroot}}/.htaccess {{deploy_path}}/shared/{{typo3_webroot}}');
    run('cp {{release_path}}/config/system/settings.php {{deploy_path}}/shared/config/system');
});

/**
 * Configure "deploy" task group.
 */
task('deploy:update_code')->hidden()->disable();

desc('Deploys a TYPO3 project');
task('deploy', [
    'deploy:info',
    'deploy:setup',
    'deploy:lock',
    'deploy:release',
    'rsync',
    'deploy:shared',
    'deploy:writable',
    'deploy:vendors',
    'typo3:setup',
    'typo3:extension:setup',
    'typo3:copy:shared',
    'deploy:symlink',
    'deploy:unlock',
    'deploy:cleanup',
    'deploy:success',
]);

after('deploy:failed', 'deploy:unlock');
